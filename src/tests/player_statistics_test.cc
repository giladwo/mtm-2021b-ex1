#include <gtest/gtest.h>

extern "C" {
    #include "../chess/mtm_map/map.h"

    #include "../chess/chessSystem.h"
    #include "../chess/playerStatistic.h"
}

TEST(PlayerStatistics, TestCreateDestroy) {
    PlayerStatistic const statistics =
            playerStatisticCreate(1, 0, 0, 0, 1);
    ASSERT_TRUE(statistics);
    playerStatisticDestroy(statistics);
}

TEST(PlayerStatistics, TestCompareByRank) {
    PlayerStatistic const statistics1 =
            playerStatisticCreate(4, 3, 0, 0, 1);
    ASSERT_TRUE(statistics1);

    PlayerStatistic const statistics2 =
            playerStatisticCreate(3, 3, 1, 0, 1);
    ASSERT_TRUE(statistics2);

    PlayerStatistic const statistics3 =
            playerStatisticCreate(2, 3, 1, 1, 1);
    ASSERT_TRUE(statistics3);

    PlayerStatistic const statistics4 =
            playerStatisticCreate(1, 3, 1, 1, 1);
    ASSERT_TRUE(statistics4);

    // 1 > 2
    ASSERT_LT(0, playerStatisticCompareByRank(statistics1, statistics2));
    // 2 < 3
    ASSERT_GT(0, playerStatisticCompareByRank(statistics2, statistics3));
    // 3 < 4
    ASSERT_GT(0, playerStatisticCompareByRank(statistics3, statistics4));

    playerStatisticDestroy(statistics4);
    playerStatisticDestroy(statistics3);
    playerStatisticDestroy(statistics2);
    playerStatisticDestroy(statistics1);
}

