#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <assert.h>
#include "part_2_1.h"

int getListLength(Node const list)
{
    assert(list);
    int length = 0;
    Node current = list;
    while (current) {
        ++length;
        current = current->next;
    }
    return length;
}

bool isListSorted(Node const list)
{
    assert(list);
    Node current = list;
    Node next = current->next;
    while (next) {
        if (current->x > next->x) {
            return false;
        }
        current = next;
        next = next->next;
    }
    return true;
}

Node nodeCreate(int const x, Node const next)
{
    Node const node = malloc(sizeof(*node));
    if (!node) {
        return NULL;
    }
    node->x = x;
    node->next = next;
    return node;
}

void listDestroy(Node const list)
{
    Node current = list;
    while (current) {
        Node const to_delete = current;
        current = current->next;
        free(to_delete);
    }
}

void listPrint(char const * const title, Node const list)
{
    Node current = list;
    printf("%s: ", title);
    while (current) {
        printf("%d->", current->x);
        current = current->next;
    }
    printf("NULL\n");
}

Node mergeSortedLists(Node const list1, Node const list2, ErrorCode * const errorCode)
{
    assert(errorCode);

    if (!(list1 && list2)) {
        *errorCode = NULL_ARGUMENT;
        return NULL;
    }

    if (!(getListLength(list1) && getListLength(list2))) {
        *errorCode = EMPTY_LIST;
        return NULL;
    }

    if (!(isListSorted(list1) && isListSorted(list2))) {
        *errorCode = UNSORTED_LIST;
        return NULL;
    }

    Node left = list1;
    Node right = list2;
    // placeholder
    Node const head = nodeCreate(-1, NULL);
    if (!head) {
        *errorCode = MEMORY_ERROR;
        return NULL;
    }
    Node current = head;
    Node next = NULL;
    while (left || right) {
        if (left && ((right && (left->x < right->x)) || !right)) {
            next = nodeCreate(left->x, NULL);
            left = left->next;
        } else if (right) {
            next = nodeCreate(right->x, NULL);
            right = right->next;
        }
        if (!next) {
            *errorCode = MEMORY_ERROR;
            listDestroy(head);
            return NULL;
        }
        current->next = next;
        current = next;
        next = NULL;
    }
    Node const result = head->next;
    head->next = NULL;
    listDestroy(head);
    return result;
}

int main()
{
    Node const l0 = nodeCreate(5, NULL);
    if (!l0) {
        return 1;
    }
    Node const l1 = nodeCreate(3, l0);
    if (!l1) {
        listDestroy(l0);
        return 2;
    }
    Node const left = nodeCreate(0, l1);
    if (!left) {
        listDestroy(l1);
        return 3;
    }

    Node const r0 = nodeCreate(51, NULL);
    if (!r0) {
        listDestroy(left);
        return 4;
    }
    Node const r1 = nodeCreate(4, r0);
    if (!r1) {
        listDestroy(left);
        listDestroy(r0);
        return 5;
    }
    Node const right = nodeCreate(2, r1);
    if (!right) {
        listDestroy(left);
        listDestroy(r1);
        return 6;
    }

    ErrorCode error = SUCCESS;
    listPrint("left", left);
    listPrint("right", right);
    Node const result = mergeSortedLists(left, right, &error);
    listPrint("result", result);
    listDestroy(result);
    listDestroy(left);
    listDestroy(right);
    return 0;
}
