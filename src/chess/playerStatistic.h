#ifndef PLAYER_STATISTICS_H
#define PLAYER_STATISTICS_H

/** Type for representing the relevant data of a player */
typedef struct player_statistic_t *PlayerStatistic;

/**
 * playerStatisticCreate: create a struct containing the relevant data
 * of a player.
 *
 * @param player_id - the ID of the player to which the data belongs.
 * @param wins -  the amount of wins the player has.
 * @param losses - the amount of losses the player has.
 * @param draws - the amount of draws a player has.
 * @param play_time - the amount of time the player has played.
 * @return A new chess system in case of success, and NULL otherwise (e.g.
 *     in case of an allocation error)
 */
PlayerStatistic playerStatisticCreate(int player_id, int wins, int losses,
                                      int draws, int play_time);

/**
 * playerStatisticDestroy: free the statistics of a player from
 * memory.
 *
 * @param player_statistics - the player statistics to free from memory. A NULL value is
 *     allowed, and in that case the function does nothing.
 */
void playerStatisticDestroy(PlayerStatistic player_statistics);

/**
 * playerStatisticCopy: copies a playerStatistics.
 *
 * @param player_statistics - the struct from which the data will be copied.
 * @return a playerStatistics with the same parameters as player_statistics, and NULL otherwise
 */
PlayerStatistic playerStatisticCopy(PlayerStatistic player_statistics);

/**
 * playerStatisticGetPlayTime: gets the play time of the player.
 *
 * @param player_statistics - the struct containing the relevant data about the player
 * @return the play time of the player, an integer.
 */
int playerStatisticGetPlayTime(PlayerStatistic player_statistics);

/**
 * playerStatisticGetMatchCount: gets the amount of matches a player has played.
 *
 * @param player_statistics - the struct containing the relevant data about the player
 * @return the amount of games the player has played, an integer.
 */
int playerStatisticGetMatchCount(PlayerStatistic player_statistics);

/**
 * playerStatisticGetID: gets the ID of the player.
 *
 * @param player_statistics - the struct containing the relevant data about the player.
 * @return the ID of the player, an int.
 */
int playerStatisticGetID(PlayerStatistic player_statistics);

/**
 * playerStatisticGetLevel: gets the scoring of the player according to wins, losses and draws
 *
 * @param player_statistics - the struct containing the relevant data about the player.
 * @return the level of the player, a double.
 */
double playerStatisticGetLevel(PlayerStatistic player_statistics);

/**
 * playerStatisticMerge: merges the data of two players.
 *
 * @param to - a struct containing the relevant data of a player from which we will merge.
 * @param from - a struct containing the relevant data of a player into which we will merge.
 */
void playerStatisticMerge(PlayerStatistic to, PlayerStatistic from);

/**
 * playerStatisticCompareByLevel: compares the players by their level.
 *
 * @param left - one of the players being compared.
 * @param right - one of the players being compared.
 * @return the difference of level between left and right.
 */
int playerStatisticCompareByLevel(PlayerStatistic left, PlayerStatistic right);

/**
 * playerStatisticCompareByRank: compares the players by their rank.
 *
 * @param left - one of the players being compared.
 * @param right - one of the players being compared.
 * @return the difference between the rank points of left and right.
 */
int playerStatisticCompareByRank(PlayerStatistic left, PlayerStatistic right);

#endif // PLAYER_STATISTICS_H
