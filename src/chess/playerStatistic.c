#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <assert.h>

#include "chessSystem.h"
#include "playerStatistic.h"
#include "match.h"

struct player_statistic_t
{
    int const player_id;
    int wins;
    int losses;
    int draws;
    int total_play_time;
};
/**
 * calculateRankPoints: calculates the rank points of a player.
 *
 * @param info - a struct with relevant data on a player.
 * @return the rank points of the player, an integer.
 */
static int calculateRankPoints(PlayerStatistic info);

PlayerStatistic playerStatisticCreate(int const player_id, int const wins,
                                      int const losses, int const draws,
                                      int const play_time)
{

    assert(matchValidatePlayerID(player_id));
    assert((0 <= wins) && (0 <= losses) && (0 <= draws) && (0 < play_time));

    PlayerStatistic const player_statistics = malloc(sizeof(*player_statistics));
    if (!player_statistics) {
        return NULL;
    }

    struct player_statistic_t tmp = {
            .player_id = player_id,
            .wins = wins,
            .losses = losses,
            .draws = draws,
            .total_play_time = play_time,
    };
    assert(sizeof(*player_statistics) == sizeof(tmp));
    memcpy(player_statistics, &tmp, sizeof(tmp));

    return player_statistics;
}

void playerStatisticDestroy(PlayerStatistic player_statistics)
{
    if (player_statistics) {
        free(player_statistics);
    }
}

PlayerStatistic playerStatisticCopy(PlayerStatistic player_statistics)
{
    if (!player_statistics) {
        return NULL;
    }

    return playerStatisticCreate(player_statistics->player_id,
                                 player_statistics->wins,
                                 player_statistics->losses,
                                 player_statistics->draws,
                                 player_statistics->total_play_time);
}

int playerStatisticGetPlayTime(PlayerStatistic player_statistics)
{
    return player_statistics ? player_statistics->total_play_time : -1;
}

int playerStatisticGetMatchCount(PlayerStatistic player_statistics)
{
    if (!player_statistics) {
        return -1;
    }
    return player_statistics->wins + player_statistics->losses + player_statistics->draws;
}

int playerStatisticGetID(PlayerStatistic player_statistics)
{
    return player_statistics ? player_statistics->player_id : -1;
}

double playerStatisticGetLevel(PlayerStatistic player_statistics)
{
    if (!player_statistics) {
        return -1;
    }
    int const matches = playerStatisticGetMatchCount(player_statistics);
    return ((double) (6 * player_statistics->wins - 10 * player_statistics->losses + 2 * player_statistics->draws)) / matches;
}

int playerStatisticCompareByLevel(PlayerStatistic left,
                                  PlayerStatistic right)
{

    if (!(left && right)) {
        assert(false);
        return 0;
    }

    // primary oder is descending level
    double const comparison = playerStatisticGetLevel(right) - playerStatisticGetLevel(left);
    // to make sure the conversion to int is correct
    if (comparison) {
        return (int) comparison;
    }
    // secondary order is ascending ID
    return left->player_id - right->player_id;
}

int playerStatisticCompareByRank(PlayerStatistic left,
                                 PlayerStatistic right)
{

    if (!(left && right)) {
        return 0;
    }

    int const left_points = calculateRankPoints(left);
    int const right_points = calculateRankPoints(right);
    int const points_comparison = left_points - right_points;
    if (points_comparison) {
        return points_comparison;
    }

    // less losses is better
    int const losses_comparison = -(left->losses - right->losses);
    if (losses_comparison) {
        return losses_comparison;
    }

    int const wins_comparison = left->wins - right->wins;
    if (wins_comparison) {
        return wins_comparison;
    }

    // The lower id has a higher rank
    return -(left->player_id - right->player_id);
}

void playerStatisticMerge(PlayerStatistic to, PlayerStatistic from)
{
    assert(to);
    if (from) {
        to->wins += from->wins;
        to->losses += from->losses;
        to->draws += from->draws;
        to->total_play_time += from->total_play_time;
    }

}

static int calculateRankPoints(PlayerStatistic const info)
{
    return 2 * info->wins + info->draws;
}
