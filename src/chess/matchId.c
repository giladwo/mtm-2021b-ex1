#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <assert.h>

#include "./mtm_map/map.h"

#include "chessSystem.h"
#include "tournament.h"
#include "playerStatistic.h"
#include "match.h"
#include "matchId.h"

struct match_id_t
{
    int const first_player;
    int const second_player;
    // Used to sort multiple matches between a specific pair of players
    // Required in order to calculate statistics taking into account matches with
    // removed players
    int const counter;
};

/**
 * matchIdCreate: creates a key for the match map.
 *
 * @param first_player - the id of the first player in the match.
 * @param second_player - the id of the second player in the match.
 * @return the key of the match map for this match.
 */
static MatchId matchIdCreate(int first_player, int second_player, int counter);

/**
 * matchIdIsMatchOf: checks if a match contains a player.
 *
 * @param match_id - the id of the match.
 * @param player_id - the id of the player.
 * @return true if the player is in the match and false otherwise.
 */
static bool matchIdIsMatchOf(MatchId match_id, int player_id);

/**
 * orderPair: assigns min to left and max to right.
 *
 * @param left - the left player
 * @param right - the right player.
 * @param min - the minimal number representing a player.
 * @param max - the maximal number representing a player.
 */
static void orderPair(int left, int right, int *min, int *max);

MatchId matchIdCreateFirst(int const first_player, int const second_player)
{
    return matchIdCreate(first_player, second_player, 0);
}

MatchId matchIdCreateNewer(MatchId const id)
{
    return id ? matchIdCreate(id->first_player, id->second_player, 1 + id->counter) : NULL;
}

/**
 * matchIdDestroy: frees the match map key.
 * @param id - the match map key to be freed.
 */
void matchIdDestroy(MatchId const id)
{
    if (id) {
        free(id);
    }
}

MatchId matchIdCopy(MatchId const id)
{
    return id ? matchIdCreate(id->first_player, id->second_player, id->counter) : NULL;
}

/// Match ids are sorted by (first_layer ASC, second_player ASC, counter DESC)
int matchIdCompare(MatchId const left, MatchId const right)
{
    assert(left && right);
    assert(left->first_player < left->second_player);
    assert(right->first_player < right->second_player);

    int const first_player_comparison = left->first_player - right->first_player;
    if (first_player_comparison) {
        return first_player_comparison;
    }

    int const second_player_comparison = left->second_player - right->second_player;
    if (second_player_comparison) {
        return second_player_comparison;
    }

    return right->counter - left->counter;
}

MatchId matchIdLastMatchBetween(int const first_player, int const second_player,
                                Map const matches)
{

    if (!matches) {
        assert(false);
        return NULL;
    }

    assert(matchValidatePlayerID(first_player));
    assert(matchValidatePlayerID(second_player));

    MAP_FOREACH(MatchId, match_id, matches) {
#ifndef NDEBUG
        int min = -1;
        int max = -1;
        orderPair(first_player, second_player, &min, &max);
        assert((min >= match_id->first_player)
                || (max >= match_id->first_player));
#endif
        if (matchIdIsMatchOf(match_id, first_player)
            && matchIdIsMatchOf(match_id, second_player)) {

            return match_id;
        }

        matchIdDestroy(match_id);
    }

    return NULL;
}

ChessResult matchIdCanPlayersPlayAnotherMatch(MatchId const id,
                                              Map const matches,
                                              int const max_games)
{

    if (!(id && matches)) {
        return CHESS_NULL_ARGUMENT;
    }

    if (!tournamentValidateMaxGames(max_games)) {
        return CHESS_INVALID_MAX_GAMES;
    }

    int first_remaining = max_games;
    int second_remaining = max_games;

    MAP_FOREACH(MatchId, match_id, matches) {
        Match const match = mapGet(matches, match_id);
        assert(match);
        if (matchIsMatchOf(match, id->first_player)) {
            --first_remaining;
            assert(0 <= first_remaining);
        }
        if (matchIsMatchOf(match, id->second_player)) {
            --second_remaining;
            assert(0 <= second_remaining);
        }
        matchIdDestroy(match_id);
    }

    return (0 < first_remaining) && (0 < second_remaining)
            ? CHESS_SUCCESS
            : CHESS_EXCEEDED_GAMES;
}

static MatchId matchIdCreate(int const first_player, int const second_player,
                             int const counter)
{

    assert(first_player != second_player);
    assert(matchValidatePlayerID(first_player));
    assert(matchValidatePlayerID(second_player));
    assert(0 <= counter);

    MatchId const id = malloc(sizeof(*id));
    if (!id) {
        return NULL;
    }

    int min = -1;
    int max = -1;
    orderPair(first_player, second_player, &min, &max);

    struct match_id_t const tmp = {
            .first_player = min,
            .second_player = max,
            .counter = counter,
    };
    assert(sizeof(*id) == sizeof(tmp));
    memcpy(id, &tmp, sizeof(*id));
    return id;
}

static void orderPair(int const left, int const right,
                      int * const min, int * const max)
{
    if (min && max) {
        if (left < right) {
            *min = left;
            *max = right;
        } else {
            *min = right;
            *max = left;
        }
    }
}

static bool matchIdIsMatchOf(MatchId const match_id, int const player_id)
{
    return match_id && ((player_id == match_id->first_player)
                        || (player_id == match_id->second_player));
}
