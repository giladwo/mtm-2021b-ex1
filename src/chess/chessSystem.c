#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <assert.h>

#include "./mtm_map/map.h"

#include "chessSystem.h"
#include "tournament.h"
#include "playerStatistic.h"
#include "match.h"

#define DEFAULT_PLAY_TIME (0)

struct chess_system_t
{
    Map const tournaments;
};

typedef int *Id;

/**
 * idCopy: copies tournament key for map.
 * @param id - tournament id
 * @return the copied id.
 */
static Id idCopy(Id id);

/**
 * idDestroy: frees the tournament map key.
 * @param id - tournament id.
 */
static void idDestroy(Id id);

/**
 * idCompare: compares to tournament ids.
 * @param left - one of the tournament ids
 * @param right -one of the tournament ids.
 * @return positive number if left is greater than right, zero if they're the same
 * and a negative number if right is greater than left.
 */
static int idCompare(Id left, Id right);

/**
 * chessGetTournament: gets a tournament through its id.
 * @param chess - the chess system in which the tournament will be searched. Cannot be NULL.
 * @param tournament_id - the id of the tournament.
 * @param error - an error enum to specify error types.
 * @return returns the tournament with the requested id, error returns:
 *     CHESS_NULL_ARGUMENT - if chess is NULL.
 *     CHESS_INVALID_ID - if tournament id is invalid.
 *     CHESS_TOURNAMENT_NOT_EXIST - if the tournament does not exist.
 *     CHESS_SUCCESS - if the function operated as expected.
 */
static Tournament chessGetTournament(ChessSystem chess, int tournament_id,
                                     ChessResult *error);

/**
 * compareByLevel: compares players by level.
 * @param left - a void pointer, will point to a playerStatistics.
 * @param right - a void pointer, will point to a playerStatistics.
 * @return a positive number if left is greater, zero if equal and negative otherwise.
 */
static int compareByLevel(const void *left, const void *right);

/**
 * mapPutErrorHandler: returns chessResult from mapResult after maPut
 * and deallocates when needed.
 *
 * @param error - the MapResult error to be converted.
 * @param tournament_statistics - a resource that can be freed from mapPut.
 * @param player_total_statistics - a resource that can be freed from mapPut
 * @return
 *     CHESS_OUT_OF_MEMORY - in case of allocation error.
 *     CHESS_SUCCESS - if function worked as expected.
 */

static ChessResult handleMapPutError(MapResult error,
                                     Map tournament_statistics,
                                     Map player_total_statistics);

ChessSystem chessCreate()
{
    ChessSystem const chess = malloc(sizeof(*chess));
    if (!chess) {
        return NULL;
    }
    Map const tournaments = mapCreate((copyMapDataElements) tournamentCopy,
                                      (copyMapKeyElements) idCopy,
                                      (freeMapDataElements) tournamentDestroy,
                                      (freeMapKeyElements) idDestroy,
                                      (compareMapKeyElements) idCompare);
    if (!tournaments) {
        free(chess);
        return NULL;
    }

    // tournaments is const, so we can't set it on the allocated heap space
    struct chess_system_t const tmp = {
        .tournaments = tournaments,
    };
    assert(sizeof(*chess) == sizeof(tmp));
    memcpy(chess, &tmp, sizeof(*chess));

    return chess;
}

void chessDestroy(ChessSystem const chess)
{
    if (chess) {
        assert(chess->tournaments);
        mapDestroy(chess->tournaments);
        free(chess);
    }
}

ChessResult chessAddTournament(ChessSystem const chess,
                               int tournament_id,
                               int const max_games_per_player,
                               const char* const tournament_location)
{

    if (!(chess && tournament_location)) {
        return CHESS_NULL_ARGUMENT;
    }

    if (!tournamentValidateTournamentID(tournament_id)) {
        return CHESS_INVALID_ID;
    }

    if (mapContains(chess->tournaments, &tournament_id)) {
        return CHESS_TOURNAMENT_ALREADY_EXISTS;
    }

    ChessResult error_tournament = CHESS_SUCCESS;
    Tournament const tournament = tournamentCreate(tournament_id,
                                                   tournament_location,
                                                   max_games_per_player,
                                                   &error_tournament);

    if (CHESS_SUCCESS != error_tournament) {
        assert(!tournament);
        return error_tournament;
    }

    assert(tournament);
    assert(tournamentGetID(tournament) == tournament_id);
    MapResult const error_map = mapPut(chess->tournaments, &tournament_id,
                                       tournament);
    tournamentDestroy(tournament);
    switch (error_map) {
        case MAP_OUT_OF_MEMORY:
            return CHESS_OUT_OF_MEMORY;
        case MAP_SUCCESS:
            return CHESS_SUCCESS;
        case MAP_NULL_ARGUMENT:
        default:
            // This shouldn't be possible
            assert(false);
            break;
    }

    return CHESS_SUCCESS;
}

ChessResult chessAddGame(ChessSystem const chess, int const tournament_id,
                         int const first_player, int const second_player,
                         Winner const winner, int const play_time)
{

    if (!chess) {
        return CHESS_NULL_ARGUMENT;
    }

    if (!(tournamentValidateTournamentID(tournament_id)
          && matchValidatePlayerID(first_player)
          && matchValidatePlayerID(second_player)
          && (first_player != second_player))) {
        return CHESS_INVALID_ID;
    }

    ChessResult error = CHESS_SUCCESS;
    Tournament tournament = chessGetTournament(chess, tournament_id, &error);
    if (CHESS_SUCCESS != error) {
        assert(CHESS_INVALID_ID != error);
        assert(CHESS_TOURNAMENT_NOT_EXIST == error);
        return error;
    }

    return tournamentAddGame(tournament, first_player, second_player, winner, play_time);
}

ChessResult chessRemoveTournament(ChessSystem const chess, int tournament_id)
{
    if (!chess) {
        return CHESS_NULL_ARGUMENT;
    }

    if (!tournamentValidateTournamentID(tournament_id)) {
        return CHESS_INVALID_ID;
    }

    MapResult const error = mapRemove(chess->tournaments, &tournament_id);
    switch (error) {
        case MAP_ITEM_DOES_NOT_EXIST:
            return CHESS_TOURNAMENT_NOT_EXIST;
        case MAP_SUCCESS:
            return CHESS_SUCCESS;
        case MAP_NULL_ARGUMENT:
        default:
            // This shouldn't be possible
            assert(false);
            break;
    }

    return CHESS_SUCCESS;
}

ChessResult chessRemovePlayer(ChessSystem const chess, int const player_id)
{
    if (!chess) {
        return CHESS_NULL_ARGUMENT;
    }

    if (!matchValidatePlayerID(player_id)) {
        return CHESS_INVALID_ID;
    }

    bool removed_successfully = false;

    MAP_FOREACH(Id, tournament_id, chess->tournaments) {
        Tournament const tournament = mapGet(chess->tournaments, tournament_id);
        assert(tournament);
        assert(tournamentGetID(tournament) == *tournament_id);
        removed_successfully |= tournamentRemovePlayer(tournament, player_id);
        idDestroy(tournament_id);
    }

    return removed_successfully ? CHESS_SUCCESS : CHESS_PLAYER_NOT_EXIST;
}

ChessResult chessEndTournament(ChessSystem const chess, int tournament_id)
{
    if (!chess) {
        return CHESS_NULL_ARGUMENT;
    }

    if (!tournamentValidateTournamentID(tournament_id)) {
        return CHESS_INVALID_ID;
    }

    Tournament const tournament = mapGet(chess->tournaments, &tournament_id);
    if (!tournament) {
        return CHESS_TOURNAMENT_NOT_EXIST;
    }
    assert(tournamentGetID(tournament) == tournament_id);

    return tournamentEnd(tournament);
}

double chessCalculateAveragePlayTime(ChessSystem const chess,
                                     int player_id,
                                     ChessResult* const chess_result)
{
    if (!chess_result) {
        return DEFAULT_PLAY_TIME;
    }

    if (!chess) {
        *chess_result = CHESS_NULL_ARGUMENT;
        return DEFAULT_PLAY_TIME;
    }

    if (!matchValidatePlayerID(player_id)) {
        *chess_result = CHESS_INVALID_ID;
        return DEFAULT_PLAY_TIME;
    }

    double total_play_time = 0;
    int total_matches = 0;

    MAP_FOREACH(Id, tournament_id, chess->tournaments) {
        Tournament const tournament = mapGet(chess->tournaments, tournament_id);
        assert(tournament);
        assert(tournamentGetID(tournament) == *tournament_id);
        idDestroy(tournament_id);

        Map const tournament_statistics = tournamentAggregatePlayerStatistics(tournament);
        if (!tournament_statistics) {
            *chess_result = CHESS_OUT_OF_MEMORY;
            return DEFAULT_PLAY_TIME;
        }

        PlayerStatistic const player_statistics = mapGet(tournament_statistics, &player_id);
        if (player_statistics) {
            assert(0 <= playerStatisticGetPlayTime(player_statistics));
            assert(0 < playerStatisticGetMatchCount(player_statistics));
            total_play_time += playerStatisticGetPlayTime(player_statistics);
            total_matches += playerStatisticGetMatchCount(player_statistics);
        }
        mapDestroy(tournament_statistics);
    }

    if (total_matches) {
        assert((0 < total_matches) && (0 < total_play_time));
        *chess_result = CHESS_SUCCESS;
        return total_play_time / total_matches;
    }

    assert(!total_play_time);
    *chess_result = CHESS_PLAYER_NOT_EXIST;
    return DEFAULT_PLAY_TIME;
}

ChessResult chessSavePlayersLevels(ChessSystem const chess, FILE* const file)
{
    if (!(chess && file)) {
        return CHESS_NULL_ARGUMENT;
    }

    Map const player_total_statistics = mapCreate((copyMapDataElements) playerStatisticCopy,
                                                  (copyMapKeyElements) idCopy,
                                                  (freeMapDataElements) playerStatisticDestroy,
                                                  (freeMapKeyElements) idDestroy,
                                                  (compareMapKeyElements) idCompare);

    if (!player_total_statistics) {
        return CHESS_OUT_OF_MEMORY;
    }

    MAP_FOREACH(Id, tournament_id, chess->tournaments) {
        Tournament const tournament = mapGet(chess->tournaments, tournament_id);
        assert(tournament && (tournamentGetID(tournament) == *tournament_id));;
        idDestroy(tournament_id);

        Map const tournament_statistics = tournamentAggregatePlayerStatistics(tournament);
        if (!tournament_statistics) {
            return CHESS_OUT_OF_MEMORY;
        }

        MAP_FOREACH(Id, player_id, tournament_statistics) {
            PlayerStatistic const player_statistics = mapGet(tournament_statistics, player_id);
            assert(player_statistics);
            playerStatisticMerge(player_statistics, mapGet(player_total_statistics, player_id));
            MapResult const error = mapPut(player_total_statistics, player_id, player_statistics);
            idDestroy(player_id);
            ChessResult chess_error = handleMapPutError(error, tournament_statistics, player_total_statistics);
            if (CHESS_SUCCESS != chess_error) {
                return chess_error;
            }
        }
        mapDestroy(tournament_statistics);
    }

    int const players = mapGetSize(player_total_statistics);
    PlayerStatistic * const all_statistics = malloc(players * sizeof(*all_statistics));
    if (!all_statistics) {
        mapDestroy(player_total_statistics);
        return CHESS_OUT_OF_MEMORY;
    }

    int i = 0;
    MAP_FOREACH(Id, player_id, player_total_statistics) {
        PlayerStatistic const info = mapGet(player_total_statistics, player_id);
        idDestroy(player_id);
        assert(info);
        all_statistics[i++] = info;
    }

    assert(players == i);
    qsort(all_statistics, players, sizeof(PlayerStatistic), compareByLevel);
    for (int rank = 0; rank < players; ++rank) {
        int const id = playerStatisticGetID(all_statistics[rank]);
        double const level = playerStatisticGetLevel(all_statistics[rank]);
        if (0 > fprintf(file, "%d %.2f\n", id, level)) {
            free(all_statistics);
            return CHESS_SAVE_FAILURE;
        }
    }
    free(all_statistics);
    mapDestroy(player_total_statistics);
    return CHESS_SUCCESS;
}

ChessResult chessSaveTournamentStatistics(ChessSystem const chess,
                                          char* const path_file)
{

    if (!chess) {
        return CHESS_NULL_ARGUMENT;
    }

    bool a_tournament_has_ended = false;
    MAP_FOREACH(Id, tournament_id, chess->tournaments) {
        Tournament const tournament = mapGet(chess->tournaments, tournament_id);
        ChessResult const error = tournamentSaveStatistics(tournament, path_file);
        switch (error) {
            case CHESS_SAVE_FAILURE:
                idDestroy(tournament_id);
                return CHESS_SAVE_FAILURE;
            case CHESS_SUCCESS:
                a_tournament_has_ended |= true;
                break;
            case CHESS_NO_TOURNAMENTS_ENDED:
                break;
            default:
                assert(false);
        }

        idDestroy(tournament_id);
    }

    return a_tournament_has_ended ? CHESS_SUCCESS : CHESS_NO_TOURNAMENTS_ENDED;
}

static Tournament chessGetTournament(ChessSystem const chess,
                                     int tournament_id,
                                     ChessResult * const error)
{

    if (!error) {
        return NULL;
    }

    if (!chess) {
        *error = CHESS_NULL_ARGUMENT;
        return NULL;
    }

    if (!tournamentValidateTournamentID(tournament_id)) {
        *error = CHESS_INVALID_ID;
        return NULL;
    }

    Tournament const tournament = mapGet(chess->tournaments, &tournament_id);
    if (!tournament) {
        *error = CHESS_TOURNAMENT_NOT_EXIST;
        return NULL;
    }

    assert(tournamentValidateTournamentID(tournament_id));
    assert(tournamentGetID(tournament) == tournament_id);
    *error = CHESS_SUCCESS;
    return tournament;
}

static int compareByLevel(const void *left, const void *right)
{
    return playerStatisticCompareByLevel(*(PlayerStatistic *) left, *(PlayerStatistic *) right);
}

static Id idCopy(Id const id)
{
    if (!id) {
        return NULL;
    }
    Id const copy = malloc(sizeof(*copy));
    if (!copy) {
        return NULL;
    }
    *copy = *id;
    return copy;
}

static void idDestroy(Id const id)
{
    if (id) {
        free(id);
    }
}

static int idCompare(Id const left, Id const right)
{
    assert(left && right);
    return *left - *right;
}

static ChessResult handleMapPutError(MapResult const error,
                                     Map const tournament_statistics,
                                     Map const player_total_statistics) {

    switch (error) {
        case MAP_SUCCESS:
            break;
        case MAP_OUT_OF_MEMORY:
            mapDestroy(tournament_statistics);
            mapDestroy(player_total_statistics);
            return CHESS_OUT_OF_MEMORY;
        case MAP_NULL_ARGUMENT:
        default:
            assert(false);
    }
    return CHESS_SUCCESS;
}
