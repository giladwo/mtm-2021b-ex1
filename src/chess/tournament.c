#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>
#include <assert.h>

#include "./mtm_map/map.h"

#include "chessSystem.h"
#include "playerStatistic.h"
#include "matchId.h"
#include "match.h"

#include "tournament.h"

#define TOURNAMENT_WINNER_PLACEHOLDER (-1)

struct tournament_t
{
    int const id;
    char const * const location;
    int const max_games_per_player;
    Map const matches;
    int winner;
};

typedef int *PlayerId;

/**
 * playerIdCopy: copies player ID.
 *
 * @param id - the player ID to be copied.
 * @return the copy of the player ID.
 */
static PlayerId playerIdCopy(PlayerId id);

/**
 * playerIdDestroy: frees the player ID.
 *
 * @param id - the player ID to be freed.
 */
static void playerIdDestroy(PlayerId id);

/**
 * playerIdCompare: compares player IDs.
 *
 * @param left - a player ID to be compared.
 * @param right - a player ID to be compared.
 * @return a positive number is left is bigger than right,
 * zero if they are the same and a negative number otherwise.
 */
static int playerIdCompare(PlayerId left, PlayerId right);

/**
 * max: returns maximum.
 *
 * @param left - an integer to be compared.
 * @param right - an integer to be compared.
 * @return - returns the maximum of the two integers.
 */
static int max(int left, int right);
static char* duplicate(char const * string);

/**
 * findNewMatchId: finds match id for the most current game between the players.
 *
 * @param tournament - the tournament from which to look for the matches.
 * @param first_player - the first player of the match.
 * @param second_player - the second player of the match.
 * @param error - a chessResult error enum to specify error types.
 * @return the new match id, error includes:
 *     CHESS_NULL_ARGUMENT - if the tournament was NULL.
 *     CHESS_INVALID_ID - if a player id was invalid.
 *     CHESS_GAME_ALREADY_EXISTS - if the players are not removed and there is no need
 *     to open another game.
 *     CHESS_SUCCESS - if function worked as planned.
 */
static MatchId findNewMatchId(Tournament tournament, int first_player,
                              int second_player, ChessResult *error);

/**
 * findNewMatchIdError:
 *      Finds errors in the return value of findNewMatchId and frees allocations when needed.
 *
 * @param error - an error value from findNewMatchId to classify errors
 * @param id - the id of the aforementioned MatchId.
 * @return
 *    CHESS_OUT_OF_MEMORY - in case of allocation failure.
 *    
 */
static ChessResult findNewMatchIdError(ChessResult error, MatchId id);

/**
 * isMatchLegal:
 *      Returns if the match has legal parameters or not along with the
 *      type of error if there is one.
 *
 * @param tournament - the tournament in which the match will be.
 * @param first_player - the first player in the match.
 * @param second_player -the second player in the match.
 * @return
 *    CHESS_NULL_ARGUMENT - if the tournament is NULL.
 *    CHESS_INVALID_ID - if one of the players has an invalid ID.
 *    CHESS_TOURNAMENT_ENDED - if the tournament has already ended.
 *    CHESS_SUCCESS - if the match is indeed legal.
 */
static ChessResult isMatchLegal(Tournament const tournament, int const first_player,
                                int const second_player);

Tournament tournamentCreate(int id, char const * location,
                            int max_games_per_player, ChessResult *error)
{

    if (!error) {
        return NULL;
    }

    if (!location) {
        *error = CHESS_NULL_ARGUMENT;
        return NULL;
    }

    if (!tournamentValidateTournamentID(id)) {
        *error = CHESS_INVALID_ID;
        return NULL;
    }

    if (!tournamentValidateLocation(location)) {
        *error = CHESS_INVALID_LOCATION;
        return NULL;
    }

    if (!tournamentValidateMaxGames(max_games_per_player)) {
        *error = CHESS_INVALID_MAX_GAMES;
        return NULL;
    }

    Tournament const tournament = malloc(sizeof(*tournament));
    if (!tournament) {
        *error = CHESS_OUT_OF_MEMORY;
        return NULL;
    }

    char * const location_copy = duplicate(location);
    if (!location_copy) {
        free(tournament);
        *error = CHESS_OUT_OF_MEMORY;
        return NULL;
    }

    Map const matches = mapCreate((copyMapDataElements) matchCopy,
                                  (copyMapKeyElements) matchIdCopy,
                                  (freeMapDataElements) matchDestroy,
                                  (freeMapKeyElements) matchIdDestroy,
                                  (compareMapKeyElements) matchIdCompare);
    if (!matches) {
        free(location_copy);
        free(tournament);
        *error = CHESS_OUT_OF_MEMORY;
        return NULL;
    }

    struct tournament_t const tmp = {
            .id = id,
            .location = location_copy,
            .max_games_per_player = max_games_per_player,
            .winner = TOURNAMENT_WINNER_PLACEHOLDER,
            .matches = matches,
    };
    assert(!matchValidatePlayerID(tmp.winner));
    assert(sizeof(*tournament) == sizeof(tmp));
    memcpy(tournament, &tmp, sizeof(tmp));
    *error = CHESS_SUCCESS;
    return tournament;
}

void tournamentDestroy(Tournament const tournament)
{
    if (tournament) {
        free((void*) tournament->location);
        mapDestroy(tournament->matches);
        free(tournament);
    }
}

Tournament tournamentCopy(Tournament const tournament)
{
    if (!tournament) {
        return NULL;
    }

    ChessResult error = CHESS_SUCCESS;
    Tournament const copy = tournamentCreate(tournamentGetID(tournament),
                                             tournamentGetLocation(tournament),
                                             tournamentGetMaxGamesPerPlayer(tournament),
                                             &error);
    if (CHESS_SUCCESS != error) {
        assert(!copy);
        return NULL;
    }
    return copy;
}

int tournamentGetID(Tournament const tournament)
{
    return tournament ? tournament->id : 0;
}

char const * tournamentGetLocation(Tournament const tournament)
{
    return tournament ? tournament->location : NULL;
}

int tournamentGetMaxGamesPerPlayer(Tournament const tournament)
{
    return tournament ? tournament->max_games_per_player : 0;
}

bool tournamentValidateTournamentID(int const tournament_id)
{
    return 0 < tournament_id;
}

bool tournamentValidateLocation(char const * const location)
{
    if (!(location && isupper(location[0]))) {
        return false;
    }

    int current = 1;
    while (location[current]) {
        if (!(islower(location[current]) || isspace(location[current]))) {
            return false;
        }
        ++current;
    }

    return true;
}

bool tournamentValidateMaxGames(int const max_games_per_player)
{
    return 0 < max_games_per_player;
}

ChessResult tournamentAddGame(Tournament const tournament,
                              int const first_player,
                              int const second_player,
                              Winner const winner,
                              int const play_time)
{


    ChessResult const match_legal_error = isMatchLegal(tournament, first_player, second_player);

    if (CHESS_SUCCESS != match_legal_error) {
        return match_legal_error;
    }

    ChessResult new_match_id_error = CHESS_SUCCESS;
    MatchId const new_match_id =
            findNewMatchId(tournament, first_player, second_player, &new_match_id_error);
    if (CHESS_SUCCESS != findNewMatchIdError(new_match_id_error, new_match_id)) {
        return new_match_id_error;
    }

    ChessResult const can_play_together =
            matchIdCanPlayersPlayAnotherMatch(new_match_id,
                                              tournament->matches,
                                              tournamentGetMaxGamesPerPlayer(tournament));
    switch (can_play_together) {
        case CHESS_EXCEEDED_GAMES:
            matchIdDestroy(new_match_id);
            return CHESS_EXCEEDED_GAMES;
        case CHESS_SUCCESS:
            break;
        case CHESS_OUT_OF_MEMORY:
            matchIdDestroy(new_match_id);
            return CHESS_OUT_OF_MEMORY;
        default:
            assert(false);
    }

    ChessResult match_error = CHESS_SUCCESS;
    Match const match = matchCreate(first_player, second_player, winner,
                                    play_time, &match_error);
    if (CHESS_SUCCESS != match_error) {
        matchIdDestroy(new_match_id);
        assert(!match);
        assert((CHESS_OUT_OF_MEMORY == match_error) || (CHESS_INVALID_PLAY_TIME == match_error));
        return match_error;
    }

    MapResult const map_error = mapPut(tournament->matches, new_match_id, match);
    matchIdDestroy(new_match_id);
    matchDestroy(match);
    switch (map_error) {
        case MAP_SUCCESS:
            return CHESS_SUCCESS;
        case MAP_OUT_OF_MEMORY:
            return CHESS_OUT_OF_MEMORY;
        case MAP_NULL_ARGUMENT:
        default:
            // This shouldn't be possible
            assert(false);
            break;
    }

    return CHESS_SUCCESS;
}

bool tournamentIsEnded(Tournament const tournament)
{
    assert(tournament);
    return matchValidatePlayerID(tournament->winner);
}

bool tournamentRemovePlayer(Tournament const tournament, int const player_id)
{
    if (!(tournament && matchValidatePlayerID(player_id) && !tournamentIsEnded(tournament))) {
        return false;
    }

    bool successfully_removed = false;

    MAP_FOREACH(MatchId, match_id, tournament->matches) {
        Match const match = mapGet(tournament->matches, match_id);
        assert(match);
        matchIdDestroy(match_id);
        if (matchIsMatchOf(match, player_id)) {
            successfully_removed |= matchRemovePlayer(match, player_id);
        }
    }

    return successfully_removed;
}

ChessResult tournamentEnd(Tournament const tournament)
{
    if (!tournament) {
        return CHESS_NULL_ARGUMENT;
    }

    if (tournamentIsEnded(tournament)) {
        return CHESS_TOURNAMENT_ENDED;
    }

    if (mapGetSize(tournament->matches) == 0){
        return CHESS_NO_GAMES;
    }

    Map const aggregate_statistics = tournamentAggregatePlayerStatistics(tournament);
    if (!aggregate_statistics) {
        return CHESS_OUT_OF_MEMORY;
    }

    PlayerId winner_id = mapGetFirst(aggregate_statistics);
    if (!winner_id) {
        mapDestroy(aggregate_statistics);
        return CHESS_OUT_OF_MEMORY;
    }
    PlayerStatistic winner = mapGet(aggregate_statistics, winner_id);
    playerIdDestroy(winner_id);
    assert(winner);
    MAP_FOREACH(PlayerId, player_id, aggregate_statistics) {
        PlayerStatistic const current = mapGet(aggregate_statistics, player_id);
        int const comparison = playerStatisticCompareByRank(winner, current);
        if (0 > comparison) {
            winner = current;
        }
        playerIdDestroy(player_id);
    }

    tournament->winner = playerStatisticGetID(winner);
    mapDestroy(aggregate_statistics);
    return CHESS_SUCCESS;
}

Map tournamentAggregatePlayerStatistics(Tournament const tournament)
{
    if (!tournament) {
        return NULL;
    }

    Map const aggregate_statistics = mapCreate((copyMapDataElements) playerStatisticCopy,
                                               (copyMapKeyElements) playerIdCopy,
                                               (freeMapDataElements) playerStatisticDestroy,
                                               (freeMapKeyElements) playerIdDestroy,
                                               (compareMapKeyElements) playerIdCompare);
    if (!aggregate_statistics) {
        return NULL;
    }

    MAP_FOREACH(MatchId, match_id, tournament->matches) {
        Match const match = mapGet(tournament->matches, match_id);
        matchIdDestroy(match_id);
        match_id = NULL;
        assert(match);

        for (int i = 0; i < MATCH_PLAYER_COUNT; ++i) {
            PlayerStatistic const statistics = matchGetStatistics(match, i);
            if (!statistics) {
                mapDestroy(aggregate_statistics);
                return NULL;
            }
            int player_id = playerStatisticGetID(statistics);
            playerStatisticMerge(statistics, mapGet(aggregate_statistics, &player_id));
            MapResult error = mapPut(aggregate_statistics, &player_id, statistics);
            playerStatisticDestroy(statistics);
            switch (error) {
                case MAP_SUCCESS:
                    break;
                case MAP_OUT_OF_MEMORY:
                    mapDestroy(aggregate_statistics);
                    return NULL;
                case MAP_NULL_ARGUMENT:
                default:
                    assert(false);
            }
        }
    }

    return aggregate_statistics;
}

ChessResult tournamentSaveStatistics(Tournament const tournament, char * const path_file)
{
    if (!(tournament && path_file)) {
        return CHESS_NULL_ARGUMENT;
    }

    if (!tournamentIsEnded(tournament)) {
        return CHESS_NO_TOURNAMENTS_ENDED;
    }

    int longest_game_time = -1;
    double cumulative_game_time = 0;
    MAP_FOREACH(MatchId, match_id, tournament->matches) {
        Match const match = mapGet(tournament->matches, match_id);
        assert(match);
        int const match_time = matchGetPlayTime(match);
        assert(0 < match_time);
        longest_game_time = max(longest_game_time, match_time);
        cumulative_game_time += match_time;

        matchIdDestroy(match_id);
    }

    // TODO figure out player_count less expensively
    Map const statistics = tournamentAggregatePlayerStatistics(tournament);
    if (!statistics) {
        return CHESS_OUT_OF_MEMORY;
    }
    int const player_count = mapGetSize(statistics);
    mapDestroy(statistics);

    int const winner = tournament->winner;
    assert(matchValidatePlayerID(winner));
    int const match_count = mapGetSize(tournament->matches);
    double const average_match_time = cumulative_game_time / match_count;

    FILE* const output = fopen(path_file, "w");
    if (!output) {
        return CHESS_SAVE_FAILURE;
    }
    int const print_error = fprintf(output, "%d\n%d\n%.2f\n%s\n%d\n%d\n", winner,
                                    (int) longest_game_time, average_match_time,
                                    tournament->location, match_count, player_count);
    int const close_error = fclose(output);
    return (print_error <= 0) || close_error ? CHESS_SAVE_FAILURE : CHESS_SUCCESS;
}

static MatchId findNewMatchId(Tournament const tournament,
                              int const first_player,
                              int const second_player,
                              ChessResult *const error)
{

    assert(error);
    if (!error) {
        return NULL;
    }

    if (!tournament) {
        *error = CHESS_NULL_ARGUMENT;
        return NULL;
    }

    if (!(matchValidatePlayerID(first_player)
            && matchValidatePlayerID(second_player))) {

        *error = CHESS_INVALID_ID;
        return NULL;
    }

    MatchId const last_match_id = matchIdLastMatchBetween(first_player,
                                                          second_player,
                                                          tournament->matches);
    if (last_match_id) {
        Match const match = mapGet(tournament->matches, last_match_id);
        if (match && matchNoPlayersWereRemoved(match)) {
            matchIdDestroy(last_match_id);
            *error = CHESS_GAME_ALREADY_EXISTS;
            return NULL;
        }

        MatchId const new_match_id = matchIdCreateNewer(last_match_id);
        matchIdDestroy(last_match_id);
        if (!new_match_id) {
            *error = CHESS_OUT_OF_MEMORY;
            return NULL;
        }

        *error = CHESS_SUCCESS;
        return new_match_id;
    }

    MatchId const new_match_id = matchIdCreateFirst(first_player, second_player);
    if (!new_match_id) {
        *error = CHESS_OUT_OF_MEMORY;
        return NULL;
    }

    *error = CHESS_SUCCESS;
    return new_match_id;
}

static PlayerId playerIdCopy(PlayerId const id)
{
    if (!id) {
        return NULL;
    }
    PlayerId const copy = malloc(sizeof(*copy));
    if (!copy) {
        return NULL;
    }
    *copy = *id;
    return copy;
}

static void playerIdDestroy(PlayerId const id)
{
    if (id) {
        free(id);
    }
}

static int playerIdCompare(PlayerId const left, PlayerId const right)
{
    assert(left && right);
    return *left - *right;
}

static int max(int const left, int const right)
{
    return left > right ? left : right;
}

static char* duplicate(char const * string)
{
    int const length = 1 + strlen(string);
    char * const copy = malloc(sizeof(*copy) * length);
    if (!copy) {
        return NULL;
    }
    memcpy(copy, string, length);
    return copy;
}

static ChessResult isMatchLegal(Tournament const tournament, int const first_player,
                                int const second_player) {

    if (!tournament) {
        return CHESS_NULL_ARGUMENT;
    }

    if (!(matchValidatePlayerID(first_player)
          && matchValidatePlayerID(second_player)
          && (first_player != second_player))) {

        return CHESS_INVALID_ID;
    }

    if (tournamentIsEnded(tournament)) {
        return CHESS_TOURNAMENT_ENDED;
    }
    return CHESS_SUCCESS;
}

static ChessResult findNewMatchIdError(ChessResult const error,
                                       MatchId const new_match_id) {

    switch (error) {
        case CHESS_SUCCESS:
            break;
        case CHESS_OUT_OF_MEMORY:
        case CHESS_GAME_ALREADY_EXISTS:
            matchIdDestroy(new_match_id);
            return error;
        case CHESS_NULL_ARGUMENT:
        case CHESS_INVALID_ID:
        default:
            assert(false);
    }
    return CHESS_SUCCESS;
}
