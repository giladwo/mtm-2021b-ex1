#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <assert.h>

#include "chessSystem.h"
#include "playerStatistic.h"
#include "match.h"

struct match_t
{
    int const first_player;
    int const second_player;
    Winner winner;
    bool removed_first_player;
    bool removed_second_player;
    int play_time;
};

/**
 * createEmptyStatistics: creates an empty data struct for a player.
 * @param player_id - the ID of the player for which it will be created.
 * @return the struct containing the data.
 */
static PlayerStatistic createEmptyStatistics(int player_id);

Match matchCreate(int const first_player, int const second_player,
                  Winner const winner, int const play_time,
                  ChessResult *error)
{

    if (!error) {
        return NULL;
    }

    if (!(matchValidatePlayerID(first_player)
          && matchValidatePlayerID(second_player)
          && (first_player != second_player))) {
        *error = CHESS_INVALID_ID;
        return NULL;
    }

    if (!matchValidatePlayTime(play_time)) {
        *error = CHESS_INVALID_PLAY_TIME;
        return NULL;
    }

    if (!((FIRST_PLAYER == winner) || (SECOND_PLAYER == winner) || (DRAW == winner))) {
        *error = CHESS_INVALID_ID;
        return NULL;
    }

    Match const match = malloc(sizeof(*match));
    if (!match) {
        *error = CHESS_OUT_OF_MEMORY;
        return NULL;
    }
    struct match_t tmp = {
        .first_player = first_player,
        .second_player = second_player,
        .winner = winner,
        .removed_first_player = false,
        .removed_second_player = false,
        .play_time = play_time,
    };
    assert(sizeof(*match) == sizeof(tmp));
    memcpy(match, &tmp, sizeof(*match));

    *error = CHESS_SUCCESS;
    return match;
}

void matchDestroy(Match const match)
{
    free(match);
}

Match matchCopy(Match const match)
{
    if (!match) {
        return NULL;
    }

    ChessResult error = CHESS_SUCCESS;
    Match const copy = matchCreate(match->first_player, match->second_player,
                                   match->winner, match->play_time, &error);

    if (CHESS_SUCCESS != error) {
        assert(!copy);
        return NULL;
    }

    return copy;
}

bool matchValidatePlayerID(int const player_id)
{
    return 0 < player_id;
}

bool matchValidatePlayTime(int const play_time)
{
    return 0 < play_time;
}

int matchGetPlayTime(Match const match)
{
    return match ? match->play_time : -1;
}

bool matchRemovePlayer(Match const match, int const player_id)
{
    if (!(match && matchValidatePlayerID(player_id))) {
        return false;
    }

    if (player_id == match->first_player) {
        if (match->removed_first_player) {
            return false;
        }
        match->removed_first_player |= true;
        return true;
    } else if (player_id == match->second_player) {
        if (match->removed_second_player) {
            return false;
        }
        match->removed_second_player |= true;
        return true;
    }

    assert(false);
    return false;
}

bool matchNoPlayersWereRemoved(Match const match)
{
    assert(match);
    return !(match->removed_first_player || match->removed_second_player);
}

PlayerStatistic matchGetStatistics(Match match, bool player_index)
{
    if (!match) {
        return NULL;
    }

    if (player_index) {
        if (match->removed_second_player) {
            return createEmptyStatistics(match->second_player);
        } else if (match->removed_first_player) {
            return playerStatisticCreate(match->second_player,
                                         1,
                                         0,
                                         0,
                                         match->play_time);
        } else {
            return playerStatisticCreate(match->second_player,
                                         SECOND_PLAYER == match->winner,
                                         FIRST_PLAYER == match->winner,
                                         DRAW == match->winner,
                                         match->play_time);
        }
    } else {
        if (match->removed_first_player) {
            return createEmptyStatistics(match->first_player);
        } else if (match->removed_second_player) {
            return playerStatisticCreate(match->first_player,
                                         1,
                                         0,
                                         0,
                                         match->play_time);
        } else {
            return playerStatisticCreate(match->first_player,
                                         FIRST_PLAYER == match->winner,
                                         SECOND_PLAYER == match->winner,
                                         DRAW == match->winner,
                                         match->play_time);
        }
    }
}

static PlayerStatistic createEmptyStatistics(int const player_id)
{
    return playerStatisticCreate(player_id, 0, 0, 0, 0);
}

bool matchIsMatchOf(Match match, int player_id)
{
    return match && (((player_id == match->first_player) && !match->removed_first_player)
                     || ((player_id == match->second_player) && !match->removed_second_player));
}
